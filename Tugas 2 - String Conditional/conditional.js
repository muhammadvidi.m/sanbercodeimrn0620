var tanggal = 2;
var bulan = 4;
var tahun = 2000;
var buatBulan;
switch (true) {
    case (tanggal < 1 || tanggal > 31):
        console.log('tanggalnya kelewat');
        break;

    case (tahun < 1900 || tahun > 2200):
        console.log('tahunnya kelewat');
        break;

    case (bulan < 1 || bulan > 12):
        console.log('bulannya kelewat');
        break;

    default:
        switch (bulan) {
            case 1:
                buatBulan = 'Januari';
                console.log(tanggal + ' ' + buatBulan + ' ' + tahun);
                break;
            case 2:
                buatBulan = 'Februari';
                console.log(tanggal + ' ' + buatBulan + ' ' + tahun);
                break;
            case 3:
                buatBulan = 'Maret';
                console.log(tanggal + ' ' + buatBulan + ' ' + tahun);
                break;
            case 4:
                buatBulan = 'April';
                console.log(tanggal + ' ' + buatBulan + ' ' + tahun);
                break;
            case 5:
                buatBulan = 'Mei';
                console.log(tanggal + ' ' + buatBulan + ' ' + tahun);
                break;
            case 6:
                buatBulan = 'Juni';
                console.log(tanggal + ' ' + buatBulan + ' ' + tahun);
                break;
            case 7:
                buatBulan = 'Juli';
                console.log(tanggal + ' ' + buatBulan + ' ' + tahun);
                break;
            case 8:
                buatBulan = 'Agustus';
                console.log(tanggal + ' ' + buatBulan + ' ' + tahun);
                break;
            case 9:
                buatBulan = 'September';
                console.log(tanggal + ' ' + buatBulan + ' ' + tahun);
                break;
            case 10:
                buatBulan = 'Oktober';
                console.log(tanggal + ' ' + buatBulan + ' ' + tahun);
                break;
            case 11:
                buatBulan = 'November';
                console.log(tanggal + ' ' + buatBulan + ' ' + tahun);
                break;
            case 12:
                buatBulan = 'Desember';
                console.log(tanggal + ' ' + buatBulan + ' ' + tahun);
                break;

            default:
                console.log('yg diinput apasi');
                break;
        }
        break;
}

var nama = 'John';
var peran = '' //penyihir, guard, werewolf

if (nama && peran == '') {
    console.log('Nama harus diisi!');
}

if (nama != '' && peran == 'penyihir') {
    console.log('Selamat datang di Dunia Werewolf ' + nama + '\n' + 'Halo ' + peran + nama + ', kamu dapat melihat siapa yang menjadi werewolf!');
}

if (nama != '' && peran == 'guard') {
    console.log('Selamat datang di Dunia Werewolf ' + nama + '\n' + 'Halo ' + peran + nama + 'kamu akan membantu melindungi temanmu dari serangan werewolf.');
}

if (nama != '' && peran == 'werewolf') {
    console.log('Selamat datang di Dunia Werewolf ' + nama + '\n' + 'Halo ' + peran + nama + 'Kamu akan memakan mangsa setiap malam!');
}

if (nama != '' && peran == '') {
    console.log('Halo ' + nama + ', Pilih peranmu untuk memulai game!');
}