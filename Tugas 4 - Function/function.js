// soal 1
function teriak() {
	return 'Halo Sanbers!';
}
console.log(teriak());
// soal 2
function kalikan(number1, number2) {
	return number1 * number2;
}
var num1 = 12;
var num2 = 4;

var hasilKali = kalikan(num1, num2);
console.log(hasilKali);

// soal 3
/* function introduce(kata1, kata2, kata3, kata4) {
	var penggabungKata =
		'Nama saya ' +
		kata1 +
		', umur saya ' +
		kata2 +
		' tahun, alamat saya di ' +
		kata3 +
		', dan saya punya hobby yaitu ' +
		kata4 +
		'!';
	return penggabungKata;
}
var name = 'Agus';
var age = 30;
var address = 'Jln. Malioboro, Yogyakarta';
var hobby = 'Gaming';

var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan); */
var penggabungKata = function kumpulanKata(kata1, kata2, kata3, kata4) {
	var gabungKata =
		'Nama saya ' +
		kata1 +
		', umur saya ' +
		kata2 +
		' tahun, alamat saya di ' +
		kata3 +
		', dan saya punya hobby yaitu ' +
		kata4 +
		'!';
	return gabungKata;
};
var name = 'Agus';
var age = 30;
var address = 'Jln. Malioboro, Yogyakarta';
var hobby = 'Gaming';

var perkenalan = penggabungKata(name, age, address, hobby);
console.log(perkenalan);
