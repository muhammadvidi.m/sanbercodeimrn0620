// Soal 1
var nomor = 0;
var fungsiAddMin = 2;
var penomoran = 22;

console.log('LOOPING PERTAMA');
while (nomor < 20) {
	nomor += fungsiAddMin;

	console.log(nomor + ' - I love coding');
}

console.log('LOOPING KEDUA');
while (penomoran > 2) {
	penomoran -= fungsiAddMin;

	console.log(penomoran + ' - I will become a mobile developer');
}

// Soal 2
console.log('OUTPUT');

for (var index = 1; index <= 20; index++) {
	if (index % 2 == 0) {
		console.log(index + ' - Berkualitas');
	} else if (index % 3 == 0) {
		console.log(index + ' - I love coding');
	} else {
		console.log(index + ' - Santai');
	}
}

// Soal 3
var pagar = '';

for (var panjang = 0; panjang < 4; panjang++) {
	for (var lebar = 0; lebar < 8; lebar++) {
		pagar += '#';
	}
	pagar += '\n';
}
console.log(pagar);

// Soal 4
var tangga = '';

for (var baris = 0; baris < 7; baris++) {
	for (var kolom = 0; kolom <= baris; kolom++) {
		tangga += '#';
	}
	tangga += '\n';
}
console.log(tangga);

// Soal 5
let chessBoard = '';
let j,
	k = 1;

for (i = 1; i < 9; i++) {
	for (j = k; j < k + 8; j++) {
		if (j % 2 == 0) {
			chessBoard += '#';
		} else {
			chessBoard += ' ';
		}
	}

	chessBoard += '\n';
	k = j + 1;
}

console.log(chessBoard);
