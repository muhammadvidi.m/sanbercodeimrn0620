// soal 1

var now = new Date()
var thisYear = now.getFullYear()
var yearNotice
var fullName = []

function arrayToObject(arr) {
    // Code di sini 
    if (arr[0] == undefined) {
        console.log('')
    } else {
        for (var looping = 0; looping < arr.length; looping++) {
            if (arr[looping][3] == undefined || arr[looping][3] > thisYear) {
                yearNotice = 'Invalid birth year'
            } else {
                var yearInNumber = thisYear - arr[looping][3]
                yearNotice = yearInNumber
            }
            var peopleDetail = {}
            peopleDetail.firstName = arr[looping][0]
            peopleDetail.lastName = arr[looping][1]
            peopleDetail.gender = arr[looping][2]
            peopleDetail.age = yearNotice
            fullName[arr[looping][0] + arr[looping][1]] = peopleDetail
            console.log(fullName)
            fullName = {}
            detail = {}
        }

    }


}

// Driver Code
var people = [
    ["Bruce", "Banner", "male", 1975],
    ["Natasha", "Romanoff", "female"]
]
arrayToObject(people)
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

var people2 = [
    ["Tony", "Stark", "male", 1980],
    ["Pepper", "Pots", "female", 2023]
]
arrayToObject(people2)
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

// Error case 
arrayToObject([]) // ""

//soal 2

var newObject = {}

function shoppingTime(memberId, money) {
    // you can only write your code here!
    var sisa

    if (!memberId || !money) {
        return 'Mohon maaf, toko X hanya berlaku untuk member saja'
    } else if (money < 50000) {
        return 'Mohon maaf, uang tidak cukup'
    }


    newObject.memberid = memberId
    newObject.money = money
    sisa = newObject.money
    container = []

    if (sisa >= 1500000) {
        container.push('Sepatu Stacattu')
        sisa -= 1500000
    }
    if (sisa >= 500000) {
        container.push('Baju Zoro')
        sisa -= 500000
    }
    if (sisa >= 250000) {
        container.push('Baju H&N')
        sisa -= 250000
    }
    if (sisa >= 175000) {
        container.push('Sweater Uniklooh')
        sisa -= 175000
    }
    if (sisa >= 50000) {
        container.push('Casing Handphone')
        sisa -= 50000
    }
    newObject.listPurchased = container
    newObject.changemoney = sisa

    sisa = 0

    return newObject

}


// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

//soal 3
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    //your code here
}

//TEST CASE
console.log(naikAngkot([
    ['Dimitri', 'B', 'F'],
    ['Icha', 'A', 'B']
]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]