// soal 1
console.log('');
console.log('Soal 1');

function range(num1, num2) {
    let array = [];
    if (num1 > num2) {
        for (let perulangan = num1; perulangan >= num2; perulangan--) {
            array.push(perulangan);
        }
        return array;
    } else if (num1 < num2) {
        for (let looping = num1; looping <= num2; looping++) {
            array.push(looping);
        }
        return array;
    } else if (num1 == undefined || num2 == undefined) {
        array.push(-1);
        return array;
    }
}
console.log(range(1, 10)); //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)); // -1
console.log(range(11, 18)); // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)); // [54, 53, 52, 51, 50]
console.log(range()); // -1

// soal 2
console.log('');
console.log('Soal 2');

function rangeWithStep(startNum, finishNum, step) {
    var container = [];
    if (startNum > finishNum) {
        for (let each = startNum; each >= finishNum; each -= step) {
            container.push(each);
        }
        return container;
    } else if (startNum < finishNum) {
        for (let loops = startNum; loops <= finishNum; loops += step) {
            container.push(loops);
        }
        return container;
    } else {
        return 'Salah isiannya ma bro';
    }
}
console.log(rangeWithStep(1, 10, 2)); // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)); // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)); // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)); // [29, 25, 21, 17, 13, 9, 5]

// soal 3
console.log('');
console.log('Soal 3');

function sum(angkaKepala, angkaTengah, nilaiPembeda) {
    let keluaran = 0;

    if (angkaKepala > angkaTengah && nilaiPembeda == undefined) {
        nilaiPembeda = 1;
        for (let pengulangan = angkaKepala; pengulangan >= angkaTengah; pengulangan -= nilaiPembeda) {
            keluaran += pengulangan;
        }
        //return keluaran
    } else if (angkaKepala < angkaTengah && nilaiPembeda == undefined) {
        nilaiPembeda = 1;
        for (let pemutar = angkaKepala; pemutar <= angkaTengah; pemutar += nilaiPembeda) {
            keluaran += pemutar;
        }
        //return keluaran
    } else if (angkaKepala > angkaTengah) {
        for (let pengulangan1 = angkaKepala; pengulangan1 >= angkaTengah; pengulangan1 -= nilaiPembeda) {
            keluaran += pengulangan1;
        }
    } else if (angkaKepala < angkaTengah) {
        for (let pemutar2 = angkaKepala; pemutar2 < angkaTengah; pemutar2 += nilaiPembeda) {
            keluaran += pemutar2;
        }
    } else if (angkaKepala && angkaTengah == undefined) {
        keluaran += 1;
    }
    return keluaran;
}
console.log(sum(1, 10)); // 55
console.log(sum(5, 50, 2)); // 621
console.log(sum(15, 10)); // 75
console.log(sum(20, 10, 2)); // 90
console.log(sum(1)); // 1
console.log(sum()); // 0

// soal 4
console.log('');
console.log('Soal 4');

function dataHandling(data) {
    //let combine;
    //data.forEach(element => {
    /* combine = `Nomor ID : ${element[0]}
                   Nama Lengkap : ${element[1]}
                   TTL : ${element[2]} ${element[3]}
                   Hobi : ${element[4]}\n`
    });
    return combine */
    //let combine;
    var input = [
        ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
        ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
        ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
        ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
    ]

    /*for (let iterationX = 0; iterationX < input.length; iterationX++) {
        for (let iterationY = 0; iterationY < input[iterationX].length; iterationY++) {
            //combine = 'Nomor ID : ${[input[0]} + '\n' + 'Nama : ${input[1]}' + '\n' + 'TTL : ${input[2]} ${input[3]}' + '\n' + 'Hobi : ${input[4]}'
            combine = `Nomor ID : ${input[iterationX][0]} + '\n' + Nama : ${[input[0][1]]} + '\n' + TTL : ${input[0][2]} ${input[0][3]} + '\n' + Hobi : ${input[0][4]}`

        }
        return combine
    }*/
    for (let i = 0; i < input.length; i++) {
        var arrayLength = input[i].length
        console.log(arrayLength)


        for (let j = 0; j < arrayLength; j++) {
            //console.log('Nomor ID: ' + input[i][j] + '\n' + 'Nama Lengkap: ' + input[i][j] + '\n' + 'TTL: ' + input[i][j] + input[i][j] + '\n' + 'Hobi: ' + input[i][j])
            /*let nomor = 'Nomor ID: ' + input[0][j]
            let nama = 'Nama Lengkap: ' + input[1][j]
            let lahir = 'TTL: ' + input[2][j] + input[3][j]
            let hobi = 'Hobi: ' + input[4][j]*/
            console.log('Nomor ID: ' + input[0][j])
            //console.log('Nama Lengkap: ' + input[0][j])
        }
        //console.log(nomor + '\n' + nama + '\n' + lahir + '\n' + hobi)

    }
}

dataHandling()

// soal 5
console.log('');
console.log('Soal 5');

function balikKata(str) {
    let newString = '';
    for (let search = str.length - 1; search >= 0; search--) {
        newString += str[search];
    }
    return newString
}
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

// soal 6
console.log('');
console.log('Soal 6');

function dataHandling2(params) {

}
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);